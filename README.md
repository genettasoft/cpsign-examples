# CPSign API USAGE GUIDE #

This repo is meant to serve as a basics for how to get up and running with **CPSign** using the API. 

## Current supported versions ##
- 0.7.0 : Tag v0.7.0, Master branch
- 0.6.0 : Tag v0.6.0
- 0.5.5 : Tag v0.5.5
- 0.5.0 : Tag v0.5.0
- 0.4.0 : Tag v0.4.0
- 0.3.16 : Tag v0.3.16
- 0.3.15 : Tag v0.3.15
- 0.3.14 : Tag v0.3.14
- 0.3.10 : Tag v0.3.10
- 0.3.8  : Tag v0.3.8

## How to set it up ##

### 1. Download CPSign Jar-file
Currently the only way to get CPSign is to contact GenettaSoft, a download page will come shortly. The CPSign Jar also comes with a separate jar-file with JavaDoc for getting in-editor help, setting this up in your favorit IDE should be easy (see [Setting up JavaDoc with Eclipse](http://cpsign-docs.genettasoft.com/sections/javadoc_setup.html)).

### 2. Get your license
Contact GenettaSoft to get your license, see more information at the [CPSign Documentation](http://cpsign-docs.genettasoft.com/sections/license.html). 

### 3. Clone/fork this repo
If your are using git; clone or fork this repository. In Eclipse, right click and chose "Import..." -> "Git/Projects from Git" -> "Existing local repository" (or "Clone URI" and paste the URI of this repository). 

Otherwise download the project as zip-file from the [downloads page](https://bitbucket.org/genettasoft/cpsign-examples/downloads). Once you have the source, import it in your editor. In Eclipse, right click and chose "Import..." -> "Existing Projects into Workspace" and in the next window chose "Select archive file" and browse to the zip-file you downloaded. Chose the project (should only be one) and press "Finish".

### 4. Add the CPSign-jar and edit 
Once set up in your IDE, there will be complaints about a missing required library, which is the CPSign-jar that you should have downloaded in step 1 above. The project `.classpath` refers to lib/cpsign-withdeps-0.3.5.jar, either resolve it by creating a lib-folder and adding your version of the jar-file to it (and possibly change the classpath if your version differs compared to the one used here) or simply change the classpath to point to your existing jarfile. When the classpath has been changed, there should be no further errors in the project.

Before you can run the project, you will have to point the CPSignFactory-instantiations to your CPSign-license. Currently all classes points to the location `/resources/licenses/cpsign0.3-standard.license`, redirect it to your license or add your license to the project. 

#### 5. Run and enjoy
Now you should be fully set up and everything should be running without any problems. 


## Who do I talk to? ##
Do you have any further issues, refer to the [CPSign Documentation](http://cpsign-docs.genettasoft.com/index.html) or contact GenettaSoft info@genettasoft.com