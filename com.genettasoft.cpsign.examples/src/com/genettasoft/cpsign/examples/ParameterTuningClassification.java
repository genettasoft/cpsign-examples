package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URISyntaxException;
import java.util.Arrays;

import javax.naming.CannotProceedException;

import com.genettasoft.auth.InvalidLicenseException;
import com.genettasoft.modeling.CPSignFactory;
import com.genettasoft.modeling.cheminf.SignaturesCPClassification;
import com.genettasoft.modeling.gridsearch.GridSearch;
import com.genettasoft.modeling.gridsearch.GridSearch.OptimizationType;
import com.genettasoft.modeling.gridsearch.GridSearchException;
import com.genettasoft.modeling.gridsearch.GridSearchResult;
import com.genettasoft.modeling.ml.cp.acp.ACPClassification;
import com.genettasoft.modeling.ml.ds_splitting.RandomSampling;

public class ParameterTuningClassification {

	CPSignFactory factory;
	File chemFile;


	/**
	 * Parameters to play around with 
	 */
	int nrCVFolds = 10;
	double crossValidationConfidence = 0.7;
	private int nrModels=5;
	private double calibrationRatio = 0.2;
	private double tolerance = 0.07;


	public static void main(String[] args) throws CannotProceedException, IllegalArgumentException, IllegalAccessException, IOException, GridSearchException {
		ParameterTuningClassification acp = new ParameterTuningClassification();
		acp.intialise();
		acp.tuneParameters();
		System.out.println("Finished Example Tune Parameters");
	}


	/**
	 * This method just initializes some variables and the CPSignFactory. Please change the 
	 * initialization of CPSignFactory to point to your active license. Also change the 
	 * model and signature-files into a location on your machine so that they can be used 
	 * later on, now temporary files are created for illustrative purposes. 
	 */
	public void intialise() {
		// Start with instantiating CPSignFactory with your license
		try{
			factory = new CPSignFactory(this.getClass().getResource(Configuration.STANDARD_LICENSE).toURI());
		} catch(IOException | InvalidLicenseException | URISyntaxException e){
			// Could not read the license
			Utils.writeErrAndExit(e.getMessage());
		}

		chemFile = new File(this.getClass().getResource("/resources/data/bursi_classification.sdf").getFile());
	}

	
	public void tuneParameters() throws IllegalArgumentException, IllegalAccessException, IOException, GridSearchException {
		// Create a GridSearch object 
		GridSearch gs = new GridSearch(nrCVFolds, crossValidationConfidence, tolerance);
		// Set your custom parameter regions
		gs.setC_END(6);

		// Set a Writer to write all output to (otherwise only give you the optimal result)
		Writer writer = new OutputStreamWriter(System.out);
		gs.setWriter(writer);
		
		
		// Chose your predictor and scoring algorithm
		ACPClassification predictor = factory.createACPClassification(
				factory.createLibLinearClassification(), 
				new RandomSampling(nrModels, calibrationRatio)); 

		// Wrap the predictor in Signatures-wrapper
		SignaturesCPClassification signPredictor = factory.createSignaturesCPClassification(predictor, 1, 3);

		// Load data
		try {
			signPredictor.fromChemFile(chemFile.toURI(), 
					"Ames test categorisation", 
					Arrays.asList("mutagen", "nonmutagen"));
		} catch (IllegalArgumentException | IOException e) {
			Utils.writeErrAndExit("Could not parse dataset file");
		} catch (IllegalAccessException e){
			Utils.writeErrAndExit("License does not have train access");
		}
		
		// Start the Grid Search
		GridSearchResult res = gs.classification(signPredictor, OptimizationType.EFFICIENCY);
		gs.classification(signPredictor.getProblem(), predictor, OptimizationType.EFFICIENCY);
		System.out.println(res);
	}
	


}
