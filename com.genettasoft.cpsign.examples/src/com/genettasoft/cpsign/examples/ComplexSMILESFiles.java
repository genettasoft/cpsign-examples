package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;

import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;

import com.genettasoft.auth.InvalidLicenseException;
import com.genettasoft.modeling.CPSignFactory;
import com.genettasoft.modeling.cheminf.SignaturesCPClassification;
import com.genettasoft.modeling.cheminf.SignificantSignature;
import com.genettasoft.modeling.ml.cp.tcp.TCPClassification;

public class ComplexSMILESFiles {

	
	CPSignFactory factory;
	File chemFile, tempTCPData;


	/**
	 * Parameters to play around with 
	 */
	int nrModels = 10;
	String smilesToPredict = "CCCNCC(=O)NC1=CC(=CC=C1)S(=O)(=O)NC2=NCCCCC2.C(=O)(C(=O)O)O";
	boolean saveTCPRecordsCompressed = true;


	public static void main(String[] args) throws IllegalAccessException {
		ComplexSMILESFiles tcp = new ComplexSMILESFiles();
		tcp.intialise();
		tcp.predictWithTCPClassification();
		System.out.println("Finished Example Complex SMILES files");
	}

	/**
	 * This method just initializes some variables and the CPSignFactory. Please change the 
	 * initialization of CPSignFactory to point to your active license. Also change the 
	 * model and signature-files into a location on your machine so that they can be used 
	 * later on, now temporary files are created for illustrative purposes. 
	 */
	public void intialise(){
		// Start with instantiating CPSignFactory with your license
		try {
			factory = new CPSignFactory(this.getClass().getResource(Configuration.STANDARD_LICENSE).toURI());
		} catch ( IOException | InvalidLicenseException | URISyntaxException e) {
			Utils.writeErrAndExit("Could not load the license or it was invalid");
		}

		// Init the files
		chemFile = new File(this.getClass().getResource("/resources/data/complex_smiles.smi").getFile());
		try{
			tempTCPData = File.createTempFile("bursiTCP", ".csr");
			tempTCPData.deleteOnExit();
		} catch(Exception ioe){
			Utils.writeErrAndExit("Could not create temporary files for saving models to");
		}
	}


	/**
	 * Loads previously created models and use them to predict
	 * @throws IllegalAccessException 
	 */
	public void predictWithTCPClassification() throws IllegalAccessException {

		// Chose the implementation of the TCP
		TCPClassification predictor = factory.createTCPClassification(factory.createLibLinearClassification()); 

		// Wrap the TCP-predictor in a Signatures-wrapper
		SignaturesCPClassification signPredictor = factory.createSignaturesCPClassification(predictor, 1, 3);

		// Load data from Chemical file
		try{
			
			signPredictor.fromChemFile(chemFile.toURI(), "Activity", Arrays.asList("NEG", "POS"));
		} catch (IllegalAccessException e){
			// License does not support train mode
			Utils.writeErrAndExit(e.getMessage());
		} catch (IllegalArgumentException e) {
			// Could not load any molecules from file
			Utils.writeErrAndExit(e.getMessage());
		} catch (IOException e) {
			Utils.writeErrAndExit(e.getMessage());
		}
		
		// Train!
		signPredictor.train();

		// Predict a new example
		try{
			IAtomContainer testMol = CPSignFactory.parseSMILES(smilesToPredict);
			// Get the mapping of label->p-value
			Map<String, Double> pvals = signPredictor.predictMondrian(testMol);

			System.out.println("Predicted pvals: " + pvals);

			// Predict the SignificantSignature
			SignificantSignature ss = signPredictor.predictSignificantSignature(testMol);
			System.out.println(ss);
		} catch (CDKException | IllegalArgumentException | IllegalAccessException e){
			Utils.writeErrAndExit(e.getMessage());
		}
		
		// Save the generated records and signatures to file
		try{
			signPredictor.save(tempTCPData);
		} catch(IOException | IllegalAccessException e){
			System.out.println("Could not print records and signatures");
		}

	}
	
}
