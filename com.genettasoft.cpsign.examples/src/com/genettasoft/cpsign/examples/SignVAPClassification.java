package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.naming.CannotProceedException;

import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.slf4j.LoggerFactory;

import com.genettasoft.chem.io.out.GradientFigureBuilder;
import com.genettasoft.chem.io.out.MoleculeFigure;
import com.genettasoft.chem.io.out.depictors.MoleculeGradientDepictor;
import com.genettasoft.chem.io.out.fields.PValuesField;
import com.genettasoft.chem.io.out.fields.TitleField;
import com.genettasoft.modeling.CPSignFactory;
import com.genettasoft.modeling.cheminf.SignaturesVAPClassification;
import com.genettasoft.modeling.cheminf.SignificantSignature;
import com.genettasoft.modeling.io.BNDLoader;
import com.genettasoft.modeling.ml.cv.CVMetric;
import com.genettasoft.modeling.ml.ds_splitting.FoldedSampling;
import com.genettasoft.modeling.ml.vap.avap.AVAPClassification;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

public class SignVAPClassification {

	CPSignFactory factory;
	File chemFile, tempModel;

	boolean generateImages=true;

	/**
	 * Parameters to play around with 
	 */
	int nrModels = 10, nrCVFolds = 10;
	String smilesToPredict = "CCCNCC(=O)NC1=CC(=CC=C1)S(=O)(=O)NC2=NCCCCC2";
	private String imageOutputName = "cvap_classification_gradient.png";


	public static void main(String[] args) throws CannotProceedException, IllegalAccessException {
		SignVAPClassification acp = new SignVAPClassification();
		acp.initialise();
		acp.crossvalidate();
		acp.trainAndSave();
		acp.predict();
		System.out.println("Finished Example CVAP-Classification");
	}


	/**
	 * This method just initializes some variables and the CPSignFactory. Please change the 
	 * initialization of CPSignFactory to point to your active license. Also change the 
	 * model and signature-files into a location on your machine so that they can be used 
	 * later on, now temporary files are created for illustrative purposes. 
	 */
	public void initialise() {
		// Start with instantiating CPSignFactory with your license
		Configuration.init();
		factory = Utils.getFactory();

		// Init the files
		chemFile = new File(this.getClass().getResource("/resources/data/bursi_classification.sdf").getFile());
		try{
			tempModel = File.createTempFile("bursiModels", ".cpsign");
			tempModel.deleteOnExit();
		} catch(IOException ioe){
			Utils.writeErrAndExit("Could not create temporary files for saving models in");
		}

		// Disable logging from CPSign
		Logger cpsignRoot = (Logger) LoggerFactory.getLogger("com.genettasoft");
		cpsignRoot.setLevel(Level.OFF);
	}


	/**
	 * Loads data, trains models and save the models to disc 
	 * @throws IllegalAccessException 
	 */
	public void trainAndSave() throws IllegalAccessException {

		// Chose your implementation of the ICP models (LibLinear or LibSVM)
		AVAPClassification predictor = factory.createVAPClassification(
				factory.createLibLinearClassification(), 
				new FoldedSampling(nrModels)); 

		// Wrap the CVAP-predictor in a Signatures-wrapper
		SignaturesVAPClassification signPredictor = factory.createSignaturesVAPClassification(predictor, 1, 3);

		// Load data
		try {
			signPredictor.fromChemFile(chemFile.toURI(), 
					"Ames test categorisation", 
					Arrays.asList("mutagen", "nonmutagen"));
		} catch (IllegalArgumentException | IOException e) {
			Utils.writeErrAndExit("Could not parse dataset file");
		} catch (IllegalAccessException e){
			Utils.writeErrAndExit("License does not have train access");
		}
		// or signPredictor.fromMolsIterator(molsIterator);

		// Train the aggregated ICPs
		try{
			signPredictor.train();
		} catch(IllegalAccessException e){
			Utils.writeErrAndExit(e.getMessage());
		}

		// If images should be generated
		if(generateImages){
			try{
				signPredictor.computePercentiles(chemFile.toURI());
			} catch(Exception e){
				Utils.writeErrAndExit(e.getMessage());
			}
		}

		// Save the trained models
		try {
			signPredictor.save(tempModel);
		} catch (IOException e) {
			Utils.writeErrAndExit("Could not write models");
		} catch (IllegalAccessException e) {
			Utils.writeErrAndExit("License does not have train access");
		}

	}

	/**
	 * Loads previously created models and use them to predict
	 * @throws IllegalAccessException 
	 */
	public void predict() throws IllegalAccessException {

		SignaturesVAPClassification signPredictor = null;

		// Load models previously trained
		try{
			// when the model isn't encrypted, just send null as EncryptionSpecification
			signPredictor = (SignaturesVAPClassification) BNDLoader.loadModel(tempModel.toURI(), null);
		} catch(IOException e){
			// Could not load precomputed models - try to train again
			System.err.println(e.getMessage());
			Utils.writeErrAndExit("Could not laod models and signatures previously trained");

		} catch(InvalidKeyException e){
			Utils.writeErrAndExit("Could not load encrypted model");
		}

		// Predict a new example
		IAtomContainer testMol = null;
		try{
			testMol = CPSignFactory.parseSMILES(smilesToPredict);
		} catch(IllegalArgumentException e){
			Utils.writeErrAndExit("Could not parse the smiles: " + smilesToPredict);
		}

		try {
			// Get the mapping of label->probability
			Map<String, Double> probabilities = signPredictor.predictProbabilities(testMol);

			System.out.println("Predicted probabilities: " + probabilities);

			// Predict the SignificantSignature
			SignificantSignature ss = signPredictor.predictSignificantSignature(testMol);
			System.out.println("Significant signautre="+ss.getSignature() + ", height=" + ss.getHeight() + ", atoms=" + ss.getAtoms());

			if(generateImages){
				try{
					GradientFigureBuilder imgBuilder = new GradientFigureBuilder(new MoleculeGradientDepictor());
					imgBuilder.addFieldOverImg(new TitleField(smilesToPredict));
					imgBuilder.addFieldUnderImg(new PValuesField(probabilities));
					MoleculeFigure img = imgBuilder.build(testMol, ss.getMoleculeGradient());
					File imgFile = new File(new File("").getAbsolutePath(),Configuration.IMAGE_BASE_PATH+imageOutputName);
					img.saveToFile(imgFile);
					System.out.println("Printed prediction image to: " + imgFile);
				} catch(Exception e){
					System.err.println("Could not generate image");
				}
			}

		} catch (IllegalAccessException | CDKException e) {
			// Either problem with CDK or not authorized /no models/signatures loaded
			Utils.writeErrAndExit(e.getMessage());
		}

	}

	private void crossvalidate() throws IllegalAccessException {
		// Chose your implementation of the ICP models (LibLinear or LibSVM)
		AVAPClassification predictor = factory.createVAPClassification(
				factory.createLibLinearClassification(), 
				new FoldedSampling(nrModels)); 

		// Wrap the CVAP-predictor in a Signatures-wrapper
		SignaturesVAPClassification signPredictor = factory.createSignaturesVAPClassification(predictor, 1, 3);

		// Load data
		try {
			signPredictor.fromChemFile(chemFile.toURI(), 
					"Ames test categorisation", Arrays.asList("mutagen", "nonmutagen"));

			//Do cross-validation with nrCVFolds folds
			List<CVMetric> result = signPredictor.crossvalidate(nrCVFolds); 
			System.out.println("Cross-validation with " + nrCVFolds +":");
			for (CVMetric met: result)
				System.out.println(met.toString());
		} catch (IllegalAccessException e) {
			Utils.writeErrAndExit(e.getMessage());
		} catch (IllegalArgumentException | IOException e) {
			Utils.writeErrAndExit("Could not load the data file in cross-validate");
		} 


	}


}
