package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.List;

import com.genettasoft.modeling.CPSignFactory;
import com.genettasoft.modeling.data.Problem;
import com.genettasoft.modeling.data.SparseFeature;
import com.genettasoft.modeling.io.BNDLoader;
import com.genettasoft.modeling.io.ModelInfo;
import com.genettasoft.modeling.ml.cp.CPRegressionResult;
import com.genettasoft.modeling.ml.cp.acp.ACPRegression;
import com.genettasoft.modeling.ml.cv.CVMetric;
import com.genettasoft.modeling.ml.ds_splitting.FoldedSampling;
import com.genettasoft.modeling.ml.ds_splitting.RandomSampling;

public class NumericACPRegression {

	CPSignFactory factory;
	File tempModels;
	String sparseDataPath = "/resources/sparse_data/housing_scale_small.svm";

	/**
	 * Parameters to play around with 
	 */
	int nrModels = 10;
	List<Double> crossValidationConfidences = Arrays.asList(0.7);
	double calibrationRatio = 0.2;
	int crossValidationFolds = 10;


	public static void main(String[] args) throws Exception {
		NumericACPRegression example = new NumericACPRegression();
		example.intialise();
		example.crossvalidate();
		example.trainAndSavePredictor();
		example.predict();

		System.out.println("Finished Example Numeric ACP-Regression");
	}

	/**
	 * This method just initializes some variables and the CPSignFactory. Please change the 
	 * initialization of CPSignFactory to point to your active license. Also change the 
	 * model and signature-files into a location on your machine so that they can be used 
	 * later on, now temporary files are created for illustrative purposes. 
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	public void intialise() {
		// Start with instantiating CPSignFactory with your license
			factory = Utils.getFactory();

		// Init the file
		try{
			tempModels = File.createTempFile("acp_regression.models", ".liblinear");
			tempModels.deleteOnExit();
		} catch(IOException ioe){
			Utils.writeErrAndExit("Could not create temporary files for saving models in");
		}
	}

	/**
	 * Loads data, trains models and save the models to disc 
	 */
	public void trainAndSavePredictor() {

		// Chose your predictor and scoring algorithm
		ACPRegression predictor = factory.createACPRegression(
				factory.createLibLinearRegression(),  // or use factory.createLibSvmRegression()
				new FoldedSampling(nrModels)); // Folded -> CCP, Random -> ACP

		// Load sparse data
		try{
			Problem data = Problem.fromSparseFile(this.getClass().getResourceAsStream(sparseDataPath));

			// Train the aggregated ICPs
			predictor.train(data);

			// Save models - no need to train the same models again
			predictor.setModelInfo(new ModelInfo("ACP Regression")); // Minimum requirement is to set the "model name"
			predictor.save(tempModels);

		} catch(IllegalAccessException e){
			// License not supporting training functionality 
			Utils.writeErrAndExit(e.getMessage());
		} catch (IOException e){
			Utils.writeErrAndExit("Problem loading data or saving models");
		}
	}

	/**
	 * Loads previously created models and use them to predict
	 */
	public void predict() {

		// Load models previously trained
		ACPRegression predictor = null;
		try{
			predictor = (ACPRegression) BNDLoader.loadModel(tempModels.toURI(), null);
		} catch(IOException | InvalidKeyException e){
			// Could not load precomputed models
			System.err.println(e.getMessage());
			Utils.writeErrAndExit("Could not laod models previously trained");
		}


		// Predict a new example
		List<SparseFeature> example = CPSignFactory.getSparseVector("1:-1 2:-0.64 3:-0.86437 4:-1 5:-0.37037 6:0.155011 7:0.283213 8:-0.461594 9:-1 10:-0.583969 11:-0.425532 12:1 13:-0.82064");
		// or CPSignFactory.getSparseVector(new double[]{1, 3.5, 4.1, 21.3, 64.4});
		// or CPSignFactory.getSparseVector(new int[]{1, 5, 10, 11}, new double[] {3.4, 12.2, 12.3, 5});

		try{
			List<CPRegressionResult> regResult = predictor.predict(example, Arrays.asList(0.5, 0.7, 0.9));
			for (CPRegressionResult res: regResult) {
				System.out.println("Confidence: " + res.getConfidence() + ", value: " + res.getInterval());
			}

			//Predict interval specified as distance to predicted value
			CPRegressionResult distanceResult = predictor.predictDistances(example, Arrays.asList(1.5)).get(0);
			System.out.println("Distance prediction: " + distanceResult);
			
		} catch (IllegalAccessException e){
			Utils.writeErrAndExit(e.getMessage());
		} 
	}

	public void crossvalidate() {
		// Chose your predictor and scoring algorithm
		ACPRegression predictor = factory.createACPRegression(
				factory.createLibLinearRegression(),
				new RandomSampling(nrModels, calibrationRatio));

		// Load data (do not have to load data separately for cross-validate and train/predict-part!)
		try{
			Problem data = Problem.fromSparseFile(this.getClass().getResourceAsStream(sparseDataPath));

			List<CVMetric> result = predictor.crossvalidate(data, crossValidationFolds, crossValidationConfidences);
			System.out.println("Cross-validation with " + crossValidationFolds + " folds and conficence " + crossValidationConfidences +": " + result);
		} catch (IOException e){
			Utils.writeErrAndExit("Could not load the datafile");
		}
	}

}