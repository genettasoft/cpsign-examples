package com.genettasoft.cpsign.examples;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import com.genettasoft.auth.InvalidLicenseException;
import com.genettasoft.modeling.CPSignFactory;

public class Utils {

	public static CPSignFactory getFactory() {
		try {
			return getFactory(Utils.class.getClass().getResource(Configuration.STANDARD_LICENSE).toURI());
		} catch ( URISyntaxException e) {
			Utils.writeErrAndExit(e.getMessage());
			return null; // never happening
		}
	}

	public static CPSignFactory getFactory(URI license) {
		// Instantiating CPSignFactory with your license
		try{
			return new CPSignFactory(license);
		} catch (IOException | InvalidLicenseException e){
			// Could not load the license (or it's incorrect/invalid)
			Utils.writeErrAndExit(e.getMessage());
			return null; // never happening
		}
	}

	public static void writeErrAndExit(String errMsg){
		System.err.println(errMsg);
		System.exit(1);
	}
}
