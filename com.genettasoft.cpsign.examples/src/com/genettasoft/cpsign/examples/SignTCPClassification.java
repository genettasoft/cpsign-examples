package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;

import com.genettasoft.modeling.CPSignFactory;
import com.genettasoft.modeling.cheminf.SignaturesCPClassification;
import com.genettasoft.modeling.cheminf.SignificantSignature;
import com.genettasoft.modeling.ml.cp.tcp.TCPClassification;

public class SignTCPClassification {

	CPSignFactory factory;
	File chemFile, tempTCPData;


	/**
	 * Parameters to play around with 
	 */
	String smilesToPredict = "CCCNCC(=O)NC1=CC(=CC=C1)S(=O)(=O)NC2=NCCCCC2";


	public static void main(String[] args) throws IllegalAccessException {
		SignTCPClassification example = new SignTCPClassification();
		example.intialise();
		example.predict();
		System.out.println("Finished Example TCP-Classification");
	}

	/**
	 * This method just initializes some variables and the CPSignFactory. Please change the 
	 * initialization of CPSignFactory to point to your active license. Also change the 
	 * model and signature-files into a location on your machine so that they can be used 
	 * later on, now temporary files are created for illustrative purposes. 
	 */
	public void intialise(){
		// Start with instantiating CPSignFactory with your license
		factory = Utils.getFactory();

		// Init the files
		chemFile = new File(this.getClass().getResource("/resources/data/bursi_classification.sdf").getFile());
		try{
			tempTCPData = File.createTempFile("bursiTCP", ".csr");
			tempTCPData.deleteOnExit();
		} catch(Exception ioe){
			Utils.writeErrAndExit("Could not create temporary files for saving models to");
		}
	}


	/**
	 * Loads previously created models and use them to predict
	 * @throws IllegalAccessException 
	 */
	public void predict() throws IllegalAccessException {

		// Init TCP and chose the scoring implementation
		TCPClassification predictor = factory.createTCPClassification(factory.createLibLinearClassification());

		// Wrap the predictor in a Signatures-wrapper
		SignaturesCPClassification signPredictor = factory.createSignaturesCPClassification(predictor, 1, 3);

		// Load data from Chemical file
		try{
			signPredictor.fromChemFile(chemFile.toURI(), 
					"Ames test categorisation", 
					Arrays.asList("mutagen", "nonmutagen"));
			// or signTCP.fromMolsIterator(molsIterator); if other data sources 
		} catch (IllegalAccessException e){
			// License does not support train mode
			Utils.writeErrAndExit(e.getMessage());
		} catch (IllegalArgumentException e) {
			// Could not load any molecules from file
			Utils.writeErrAndExit(e.getMessage());
		} catch (IOException e) {
			Utils.writeErrAndExit(e.getMessage());
		}

		// Train!
		signPredictor.train();

		// Predict a new example
		try{
			IAtomContainer testMol = CPSignFactory.parseSMILES(smilesToPredict);
			// Get the mapping of label->p-value
			Map<String, Double> pvals = signPredictor.predictMondrian(testMol);

			System.out.println("Predicted pvals: "+pvals);

			// Predict the SignificantSignature
			SignificantSignature ss = signPredictor.predictSignificantSignature(testMol);
			System.out.println(ss);
		} catch (CDKException | IllegalArgumentException | IllegalAccessException e){
			Utils.writeErrAndExit(e.getMessage());
		}

		// Save the generated records and signatures to file
		try{
			signPredictor.save(tempTCPData);
		} catch(IOException | IllegalAccessException e){
			System.out.println("Could not print records and signatures");
		}

	}

}
