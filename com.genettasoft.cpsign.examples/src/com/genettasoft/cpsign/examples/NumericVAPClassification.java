package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.List;

import com.genettasoft.auth.InvalidLicenseException;
import com.genettasoft.modeling.CPSignFactory;
import com.genettasoft.modeling.data.Problem;
import com.genettasoft.modeling.data.SparseFeature;
import com.genettasoft.modeling.io.BNDLoader;
import com.genettasoft.modeling.io.ModelInfo;
import com.genettasoft.modeling.ml.algorithms.LibLinear;
import com.genettasoft.modeling.ml.algorithms.params.LibLinearParameters;
import com.genettasoft.modeling.ml.cv.CVMetric;
import com.genettasoft.modeling.ml.ds_splitting.RandomSampling;
import com.genettasoft.modeling.ml.vap.avap.AVAPClassification;
import com.genettasoft.modeling.ml.vap.avap.AVAPClassificationResult;

public class NumericVAPClassification {

	CPSignFactory factory;
	File tempModel;
	String sparseDataPath = "/resources/sparse_data/spambaseShuffled_small.svm";

	/**
	 * Parameters to play around with 
	 */
	int nrModels = 10;
	double calibrationRatio = 0.2;
	int crossValidationFolds = 10;


	public static void main(String[] args) {
		NumericVAPClassification example = new NumericVAPClassification();
		example.intialise();
		example.crossvalidate();
		example.trainAndSavePredictor();
		example.predict();
		System.out.println("Finished Example Numeric CVAP-Classification");
	}

	/**
	 * This method just initializes some variables and the CPSignFactory. Please change the 
	 * initialization of CPSignFactory to point to your active license. Also change the 
	 * model and signature-files into a location on your machine so that they can be used 
	 * later on, now temporary files are created for illustrative purposes. 
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	public void intialise() {
		// Start with instantiating CPSignFactory with your license
			factory = Utils.getFactory();

		// Init the file
		try{
			tempModel = File.createTempFile("cvap_classification.models", ".liblinear");
			tempModel.deleteOnExit();
		} catch(IOException ioe){
			Utils.writeErrAndExit("Could not create temporary files for saving predictor in");
		}
	}

	/**
	 * Loads data, trains models and save the models to disc 
	 */
	public void trainAndSavePredictor() {

		// Chose your predictor and scoring algorithm
		AVAPClassification predictor = new AVAPClassification(
				new LibLinear(LibLinearParameters.defaultClassification()), 
				new RandomSampling(nrModels, calibrationRatio));

		// Load sparse data
		try{
			Problem data = Problem.fromSparseFile(this.getClass().getResourceAsStream(sparseDataPath));

			// Train the aggregated 
			predictor.train(data);

			// Save models - no need to train the same models again
			predictor.setModelInfo(new ModelInfo("CVAP Classification")); // Minimum info is to set the model name
			predictor.save(tempModel);

		} catch(IllegalAccessException e){
			// License not supporting training functionality 
			Utils.writeErrAndExit(e.getMessage());
		} catch (IOException e){
			Utils.writeErrAndExit("Problem loading data or saving models");
		} catch (InvalidLicenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Loads previously created models and use them to predict
	 */
	public void predict() {

		AVAPClassification predictor = null;

		// Load models previously trained
		try{
			predictor = (AVAPClassification) BNDLoader.loadModel(tempModel.toURI(), null);
		} catch(IOException | InvalidKeyException | IllegalArgumentException e){
			// Could not load precomputed models
			System.err.println(e.getMessage());
			Utils.writeErrAndExit("Could not laod models previously trained");
		}


		// Predict a new example
		List<SparseFeature> example = CPSignFactory.getSparseVector("1:0.44 3:0.88 5:0.44 6:1.32 18:0.44 19:1.76 21:2.2 23:2.2 49:0.222 52:0.444 53:0.37 55:2.413 56:16 57:140");
		// or CPSignFactory.getSparseVector(new double[]{1, 3.5, 4.1, 21.3, 64.4});
		// or CPSignFactory.getSparseVector(new int[]{1, 5, 10, 11}, new double[] {3.4, 12.2, 12.3, 5});
		
		try{
			AVAPClassificationResult result = predictor.predict(example);
			System.out.println("Prediction: "+result);

		} catch (IllegalAccessException e){
			Utils.writeErrAndExit(e.getMessage());
		} 
	}

	public void crossvalidate() {
		// Init predictor
		AVAPClassification predictor = factory.createVAPClassification(
				factory.createLibLinearClassification(), 
				new RandomSampling(nrModels, calibrationRatio)); 

		// Load data (do not have to load data separately for cross-validate and train/predict-part!)
		try{
			Problem data = Problem.fromSparseFile(this.getClass().getResourceAsStream(sparseDataPath));

			List<CVMetric> result = predictor.crossvalidate(data, crossValidationFolds);
			System.out.println("Cross-validation with " + crossValidationFolds +": " + result);
		} catch (IOException e){
			Utils.writeErrAndExit("Could not load the datafile");
		}
	}

}
