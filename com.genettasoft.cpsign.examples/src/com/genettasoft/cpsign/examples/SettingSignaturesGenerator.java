package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;

import com.genettasoft.modeling.CPSignFactory;
import com.genettasoft.modeling.cheminf.SignaturesCPRegression;
import com.genettasoft.modeling.cheminf.signatures.SignaturesGenerator;
import com.genettasoft.modeling.cheminf.signatures.SignaturesGeneratorStandard;
import com.genettasoft.modeling.io.BNDLoader;
import com.genettasoft.modeling.ml.cp.CPRegressionResult;
import com.genettasoft.modeling.ml.cp.acp.ACPRegression;
import com.genettasoft.modeling.ml.ds_splitting.RandomSampling;

public class SettingSignaturesGenerator {

	CPSignFactory factory;
	File chemFile, tempModel;

	int nrModels = 10;
	String smilesToPredict = "CCCNCC(=O)NC1=CC(=CC=C1)S(=O)(=O)NC2=NCCCCC2";
	private double calibrationRatio = 0.2;


	public static void main(String[] args) throws IllegalAccessException {
		SettingSignaturesGenerator acp = new SettingSignaturesGenerator();
		acp.intialise();
		acp.trainAndSave();
		acp.predict();
		System.out.println("Finished Example Setting SignaturesGenerator");
	}

	/**
	 * This method just initializes some variables and the CPSignFactory. Please change the 
	 * initialization of CPSignFactory to point to your active license. Also change the 
	 * model and signature-files into a location on your machine so that they can be used 
	 * later on, now temporary files are created for illustrative purposes. 
	 */
	public void intialise() {
		// Start with instantiating CPSignFactory with your license
		factory = Utils.getFactory();

		// Init the files
		chemFile = new File(this.getClass().getResource("/resources/data/dataset_glucocorticoid.csv.smi.sdf.std_regr_nodupl.sdf").getFile());
		try{
			tempModel = File.createTempFile("hergModels.liblinear.", ".cpsign");
			tempModel.deleteOnExit();
		} catch(IOException ioe){
			Utils.writeErrAndExit("Could not create temporary file for saving model in");
		}
	}


	/**
	 * Loads data, trains models and save the models to disc 
	 * @throws IllegalAccessException 
	 */
	public void trainAndSave() throws IllegalAccessException {

		// Chose your predictor and scoring algorithm
		ACPRegression predictor = factory.createACPRegression(
				factory.createLibLinearRegression(), 
				new RandomSampling(nrModels, calibrationRatio));

		// Wrap the predictor in Signatures-wrapper
		SignaturesCPRegression signPredictor = factory.createSignaturesCPRegression(predictor, 1, 3);

		// set signatures-generator!
		// signPredictor.setSignaturesGenerator(new SignaturesGeneratorStandard()); // standard (already set by default)
		// signPredictor.setSignaturesGenerator(new SignaturesGeneratorStereo()); // stereo-signatures
		// implement your own!
		signPredictor.setSignaturesGenerator(new MyGenerator());

		// Load data, train and save model
		try{
			signPredictor.fromChemFile(chemFile.toURI(),"target");
			// or signACP.fromMolsIterator(molsIterator);

			// Train the aggregated ICPs
			signPredictor.train();

			// Save models to skip train again
			signPredictor.save(tempModel);

		} catch (IllegalAccessException e){
			// License not supporting training functionality 
			Utils.writeErrAndExit(e.getMessage());
		} catch (IOException e){
			Utils.writeErrAndExit("Problem loading data or saving models");
		}
	}

	public class MyGenerator implements SignaturesGenerator {

		@Override
		public String getName() {
			return "my own generator";
		}

		@Override
		public Map<String, Double> generateSignatures(IAtomContainer molecule, int startheight, int endheight)
				throws CDKException {
			// just wrap the standard one
			return new SignaturesGeneratorStandard().generateSignatures(molecule, startheight, endheight);
		}

		@Override
		public Map<IAtom, String> generateSignatures(IAtomContainer molecule, int height) throws CDKException {
			// just wrap the standard one
			return new SignaturesGeneratorStandard().generateSignatures(molecule, height);
		}
	}

	/**
	 * Loads previously created models and use them to predict
	 * @throws IllegalAccessException 
	 */
	public void predict() throws IllegalAccessException {


		// Load the trained predictor
		SignaturesCPRegression predictor= null;
		// Load model previously trained
		try{
			// this will give a logger.WARN message if you have your own signatures-generator set!
			predictor = (SignaturesCPRegression) BNDLoader.loadModel(tempModel.toURI(), null);

		} catch(IOException | InvalidKeyException | IllegalArgumentException e){
			// Could not load precomputed models
			System.err.println(e.getMessage());
			Utils.writeErrAndExit("Could not laod models previously trained");
		}

		// Here you must set the signatures generator so that the same signatures can be generated for new examples
		predictor.setSignaturesGenerator(new MyGenerator());

		// Predict a new example
		try{
			IAtomContainer testMol = CPSignFactory.parseSMILES(smilesToPredict);
			List<Double> confidences = Arrays.asList(0.5, 0.7, 0.9);
			List<CPRegressionResult> regResult = predictor.predict(testMol, confidences);

			for (int i=0; i<confidences.size(); i++){
				System.out.println("Confidence: " + regResult.get(i).getConfidence() + ", interval (normal): " + regResult.get(i).getInterval());
			}

		} catch (IllegalAccessException e){
			Utils.writeErrAndExit(e.getMessage());
		} catch (CDKException e){
			Utils.writeErrAndExit("CDK Exception when configuring the IAtomContainer");
		}
	}

}
