package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.List;

import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;

import com.genettasoft.modeling.CPSignFactory;
import com.genettasoft.modeling.cheminf.SignaturesCPRegression;
import com.genettasoft.modeling.cheminf.SignificantSignature;
import com.genettasoft.modeling.io.BNDLoader;
import com.genettasoft.modeling.ml.cp.CPRegressionResult;
import com.genettasoft.modeling.ml.cp.acp.ACPRegression;
import com.genettasoft.modeling.ml.cv.CVMetric;
import com.genettasoft.modeling.ml.ds_splitting.FoldedSampling;
import com.genettasoft.modeling.ml.ds_splitting.RandomSampling;

public class SignACPRegression {

	CPSignFactory factory;
	File chemFile, tempModel;

	/**
	 * Parameters to play around with 
	 */
	boolean RUN_CCP=true;
	int nrModels = 10;
	int cvFolds = 10;
	double crossValidationConfidence = 0.7;
	String smilesToPredict = "CCCNCC(=O)NC1=CC(=CC=C1)S(=O)(=O)NC2=NCCCCC2";
	double calibrationRatio = 0.2;


	public static void main(String[] args) throws IllegalAccessException {
		SignACPRegression acp = new SignACPRegression();
		acp.intialise();
		acp.crossvalidate();
		acp.trainAndSave();
		acp.predict();
		System.out.println("Finished Example ACP-Regression");
	}

	/**
	 * This method just initializes some variables and the CPSignFactory. Please change the 
	 * initialization of CPSignFactory to point to your active license. Also change the 
	 * model and signature-files into a location on your machine so that they can be used 
	 * later on, now temporary files are created for illustrative purposes. 
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	public void intialise() {
		// Start with instantiating CPSignFactory with your license
		try{
			factory = new CPSignFactory(this.getClass().getResource(Configuration.STANDARD_LICENSE).toURI());
		} catch(IllegalArgumentException| IOException | URISyntaxException e){
			// Could not load the license (or it's incorrect/invalid)
			Utils.writeErrAndExit(e.getMessage());
		}

		// Init the files
		chemFile = new File(this.getClass().getResource("/resources/data/dataset_glucocorticoid.csv.smi.sdf.std_regr_nodupl.sdf").getFile());
		try{
			tempModel = File.createTempFile("hergModels.liblinear.", ".cpsign");
			tempModel.deleteOnExit();
		} catch(IOException ioe){
			Utils.writeErrAndExit("Could not create temporary files for saving models in");
		}
	}


	/**
	 * Loads data, trains models and save the models to disc 
	 * @throws IllegalAccessException 
	 */
	public void trainAndSave() throws IllegalAccessException {

		// Chose your implementation of the ICP models (LibLinear or LibSVM)
		ACPRegression predictor = factory.createACPRegression(
				factory.createLibLinearRegression(), 
				(RUN_CCP? new FoldedSampling(nrModels) : new RandomSampling(nrModels, calibrationRatio)));

		// Wrap the ACP-predictor chosen in Signatures-wrapper
		SignaturesCPRegression signPredictor = factory.createSignaturesCPRegression(predictor, 1, 3);

		// Load data
		try{
			signPredictor.fromChemFile(chemFile.toURI(),"target");
			// or signACP.fromMolsIterator(molsIterator);

			// Train the aggregated ICPs
			signPredictor.train();

			// Save models to skip train again
			signPredictor.save(tempModel);
			// or signACP.saveModel(new FileOutputStream(tempModel), saveModelsCompressed);
			

		} catch (IllegalAccessException e) {
			// License not supporting training functionality 
			Utils.writeErrAndExit(e.getMessage());
		} catch (IOException e) {
			Utils.writeErrAndExit("Problem loading data or saving models");
		}
	}

	/**
	 * Loads previously created models and use them to predict
	 * @throws IllegalAccessException 
	 */
	public void predict() throws IllegalAccessException {
		
		// Create the Signatures-wrapper, no ACP-implementation is needed (reading that from the model)
		SignaturesCPRegression signACP = null; 

		// Load models previously trained
		try{
			signACP = (SignaturesCPRegression) BNDLoader.loadModel(tempModel.toURI(), null);
		} catch(IOException e){
			// Could not load precomputed models
			System.err.println(e.getMessage());
			Utils.writeErrAndExit("Could not laod models and signatures previously trained");
		} catch (InvalidKeyException e) {
			Utils.writeErrAndExit("Could not read encrypted model");
		} catch (IllegalArgumentException e) {
			Utils.writeErrAndExit(e.getMessage());
		}


		// Predict a new example
		try{
			IAtomContainer testMol = CPSignFactory.parseSMILES(smilesToPredict);
			List<CPRegressionResult> regResult= signACP.predict(testMol, Arrays.asList(0.5, 0.7, 0.9));
			for(CPRegressionResult res: regResult){
				System.out.println("Confidence: " + res.getConfidence() + ", value: " + res.getInterval());
			}

			//Predict interval specified as distance to predicted value
			List<CPRegressionResult> distanceResult = signACP.predictDistances(testMol, Arrays.asList(0.001));
			System.out.println("Distance prediction: " + distanceResult.get(0));

			// Predict the SignificantSignature
			SignificantSignature ss = signACP.predictSignificantSignature(testMol);
			System.out.println(ss);
		} catch (IllegalAccessException e){
			Utils.writeErrAndExit(e.getMessage());
		} catch (CDKException e){
			Utils.writeErrAndExit("CDK Exception when configuring the IAtomContainer");
		}
	}

	public void crossvalidate() throws IllegalAccessException {
		// Chose your implementation of the ICP models (LibLinear or LibSVM)
		ACPRegression predictor = factory.createACPRegression(
				factory.createLibLinearRegression(), 
				(RUN_CCP? new FoldedSampling(nrModels) : new RandomSampling(nrModels, calibrationRatio)));

		// Wrap the ACP-implementation chosen in Signatures-wrapper
		SignaturesCPRegression signPredictor = factory.createSignaturesCPRegression(predictor, 1, 3);

		// Load data (do not have to load data separately for cross-validate and train/predict-part!)
		try{
			signPredictor.fromChemFile(chemFile.toURI(), "target");

			//Do cross-validation with 10 folds
			List<CVMetric> result = signPredictor.crossvalidate(cvFolds, crossValidationConfidence);
			System.out.println("Cross-validation with " + cvFolds + " folds and conficence " + crossValidationConfidence +": " + result);
		} catch(IllegalAccessException e){
			Utils.writeErrAndExit(e.getMessage());
		} catch (IOException e){
			Utils.writeErrAndExit("Could not load the datafile");
		}
	}



}
