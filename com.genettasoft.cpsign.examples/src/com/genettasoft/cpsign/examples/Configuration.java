package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class Configuration {
	
	public static final String STANDARD_LICENSE = "/resources/licenses/cpsign0.6-standard.license";
	public static final String PRO_LICENSE = "/resources/licenses/cpsign0.6-pro.license";

	// This is taken to be a relative path to this repo, please make changes according to your preferences
	public static final String IMAGE_BASE_PATH = "output/imgs/";
	
	static {
		// Generate parent folders for output in case not present before
		try{
			File imgDir = new File(new File("").getAbsoluteFile(),IMAGE_BASE_PATH);
			FileUtils.forceMkdir(imgDir);
		} catch (IOException e){
			throw new RuntimeException("Could not create output directory for images, please give permissions to do this or remove image generation");
		}
	}
	
	public static void init() {}
	
}
