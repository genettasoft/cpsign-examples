package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.genettasoft.auth.InvalidLicenseException;
import com.genettasoft.encryption.EncryptionSpecification;
import com.genettasoft.modeling.CPSignFactory;
import com.genettasoft.modeling.cheminf.SignaturesCPClassification;
import com.genettasoft.modeling.cheminf.SignaturesCPRegression;
import com.genettasoft.modeling.data.Dataset;
import com.genettasoft.modeling.data.Problem;
import com.genettasoft.modeling.ml.cp.acp.ACPClassification;
import com.genettasoft.modeling.ml.ds_splitting.RandomSampling;

public class ManipulatingSparseDatasets {

	CPSignFactory factory, encryptionFactory;
	Dataset dataset;

	File encryptedFile;

	public static void main(String[] args) throws Exception {
		ManipulatingSparseDatasets msp = new ManipulatingSparseDatasets();
		msp.initialise();
		msp.loadProblem();
		msp.copyAndShuffle();
		msp.splitIt();
		msp.showEncryption();
		msp.testWorkflow();
	}

	public void initialise() throws IOException {
		// Start with instantiating CPSignFactory with your license
			factory = Utils.getFactory();

		// If you have a license that supports encryption
		try{
			encryptionFactory = new CPSignFactory(this.getClass().getResource(Configuration.PRO_LICENSE).toURI());
			encryptedFile = File.createTempFile("encr_problem", ".svm");
			encryptedFile.deleteOnExit();
		} catch (IOException | InvalidLicenseException | URISyntaxException e){
			// No license that supports encryption found
		}


	}

	public void loadProblem() throws IOException {
		// A Problem can be loaded easily without the need for the factory
		dataset = Dataset.fromSparseFile(this.getClass().getResourceAsStream("/resources/sparse_data/spambaseShuffled_small.svm"));

		System.out.println("Original dataset size: " + dataset.size());
	}

	public void copyAndShuffle() throws IOException{

		// If you want to make a copy:
		Dataset copiedDataset = dataset.clone();

		// The data is now identical to the first loaded Dataset
		if (!dataset.equals(copiedDataset))
			System.err.println("Datasets not matching");

		// Shuffle the data around
		copiedDataset.shuffle();

		// Now they do not match!
		if (dataset.equals(copiedDataset))
			System.err.println("Shuffled dataset should not match");

		// Print it to make sure!
		System.out.println("Shuffled data:\n" + copiedDataset);
	}

	public void splitIt() {
		// Splitting can be done by a static partitioning
		int indexToSplitAt = 2;
		Dataset[] staticPartitions = dataset.splitStatic(indexToSplitAt);
		System.out.println("Static partition 0:\n" + staticPartitions[0]);
		System.out.println("Static partition 1:\n" + staticPartitions[1]);

		// Or static by what fraction to split at
		double firstFraction = 0.3; // first set should include 30% 
		Dataset[] staticPartitionsByFraction = dataset.splitStatic(firstFraction);
		System.out.println("Static partition by fraction 0:\n" + staticPartitionsByFraction[0]);
		System.out.println("Static partition by fraction 1:\n" + staticPartitionsByFraction[1]);

		// Or use a Random partitioning 
		Dataset[] randomPartitions = dataset.splitRandom(firstFraction);
		System.out.println("Random partition 0:\n" + randomPartitions[0]);
		System.out.println("Random partition 1:\n" + randomPartitions[1]);
	}

	public void showEncryption() throws IllegalAccessException, IllegalArgumentException, FileNotFoundException, IOException, InvalidKeyException, InvalidLicenseException{
		// Check if a license with encryption was given
		if (encryptionFactory == null || !encryptionFactory.supportEncryption())
			return;

		// Get the IEncryptionSpec that allows you to encrypt stuff
		EncryptionSpecification spec = encryptionFactory.getEncryptionSpec();

		// Write encrypted file
		dataset.writeRecordsToStream(new FileOutputStream(encryptedFile), spec);

		// Load the Problem back
		Dataset fromEncryptedFile = Dataset.fromSparseFile(new FileInputStream(encryptedFile), spec);

		System.out.println("Dataset re-loaded from encrypted file:\n" + fromEncryptedFile);

		// Make sure that it matches the original problem!
		if (!fromEncryptedFile.equals(dataset))
			System.err.println("Loaded dataset should equal saved dataset");
	}

	/**
	 * Here's an example of how to use the Sparse Predictor, we use the Signature-wrapper to compute Problems using
	 * Signatures Generation. Use the signatures generated in the first file to get the same indexes!
	 */
	public void testWorkflow() throws IllegalArgumentException, IllegalAccessException, IOException{
		// Do the signature generation step for training data
		SignaturesCPClassification signACP = factory.createSignaturesCPClassification(null, 1, 3);
		signACP.fromChemFile(new File(this.getClass().getResource("/resources/data/bursi_classification.sdf").getFile()).toURI(),
				"Ames test categorisation", 
				Arrays.asList("mutagen", "nonmutagen"));

		// Get the (Sparse) Problem
		Problem generatedProblem = signACP.getProblem().clone();
		if (generatedProblem.getDataset().isEmpty())
			throw new RuntimeException("generated dataset must not be empty");
		System.out.println("Generated problem of size: " + generatedProblem.getNumRecords());

		// Now we need some data to test with! Generate it from the other ChemFile


		List<String> signatures = signACP.getProblem().getSignatures(); 

		SignaturesCPRegression acpReg = factory.createSignaturesCPRegression(null, 1, 3); // we dont need the ACP-implementation!
		acpReg.getProblem().setSignatures(signatures); // use the previous signatures

		acpReg.fromChemFile(new File(this.getClass().getResource("/resources/data/dataset_glucocorticoid.csv.smi.sdf.std_regr_nodupl.sdf").getFile()).toURI(), "target");
		
		Problem testProblem = acpReg.getProblem().clone();

		// now we have two Problems, one for training and one for testing
		ACPClassification impl = factory.createACPClassification(factory.createLibLinearClassification(), new RandomSampling(10, 0.2));
		impl.train(generatedProblem);

		// predict the first 10 molecules 
		for(int i=0; i< 10; i++){
			Map<Integer, Double> pvals = impl.predict(testProblem.getDataset().getRecords().get(i).getFeatures());
			System.out.println("prediction: "+pvals);
		}

	}

}
