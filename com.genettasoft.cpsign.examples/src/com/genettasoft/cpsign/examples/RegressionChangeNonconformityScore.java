package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.List;

import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;

import com.genettasoft.auth.InvalidLicenseException;
import com.genettasoft.modeling.CPSignFactory;
import com.genettasoft.modeling.cheminf.SignaturesCPRegression;
import com.genettasoft.modeling.cheminf.SignificantSignature;
import com.genettasoft.modeling.io.BNDLoader;
import com.genettasoft.modeling.ml.cp.CPRegressionResult;
import com.genettasoft.modeling.ml.cp.acp.ACPRegression;
import com.genettasoft.modeling.ml.cp.nonconf.NonconfMeasureFactory;
import com.genettasoft.modeling.ml.cv.CVMetric;
import com.genettasoft.modeling.ml.ds_splitting.RandomSampling;

public class RegressionChangeNonconformityScore {

	CPSignFactory factory;
	File chemFile, tempModel, tempModelDifferentNonconfMeasure;


	/**
	 * Parameters to play around with 
	 */
	int nrModels = 10;
	int cvFolds = 10;
	double crossValidationConfidence = 0.7;
	String smilesToPredict = "CCCNCC(=O)NC1=CC(=CC=C1)S(=O)(=O)NC2=NCCCCC2";
	boolean saveModelsCompressed = true;
	private double calibrationRatio = 0.2;
	private double cvConfidence = 0.7;


	public static void main(String[] args) throws IllegalAccessException {
		RegressionChangeNonconformityScore acp = new RegressionChangeNonconformityScore();
		acp.intialise();
//		acp.crossvalidate();
		acp.trainAndSave();
		acp.predict();
		System.out.println("Finished Example ACP-Regression");
	}

	/**
	 * This method just initializes some variables and the CPSignFactory. Please change the 
	 * initialization of CPSignFactory to point to your active license. Also change the 
	 * model and signature-files into a location on your machine so that they can be used 
	 * later on, now temporary files are created for illustrative purposes. 
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	public void intialise() {
		// Start with instantiating CPSignFactory with your license
		try{
			factory = new CPSignFactory(this.getClass().getResource(Configuration.STANDARD_LICENSE).toURI());
		} catch(IOException | InvalidLicenseException | URISyntaxException e){
			// Could not load the license (or it's incorrect/invalid)
			Utils.writeErrAndExit(e.getMessage());
		}

		// Init the files
		chemFile = new File(this.getClass().getResource("/resources/data/dataset_glucocorticoid.csv.smi.sdf.std_regr_nodupl.sdf").getFile());
		try{
			tempModel = File.createTempFile("hergModels.liblinear.", ".cpsign");
			tempModelDifferentNonconfMeasure = File.createTempFile("hergModels.liblinear.", ".cpsign");
			tempModel.deleteOnExit();
			tempModelDifferentNonconfMeasure.deleteOnExit();
		} catch(IOException ioe){
			Utils.writeErrAndExit("Could not create temporary files for saving models in");
		}
	}


	/**
	 * Loads data, trains models and save the models to disc 
	 * @throws IllegalAccessException 
	 */
	public void trainAndSave() throws IllegalAccessException {

		// Chose your predictor and scoring algorithm
		ACPRegression predictor = factory.createACPRegression(
				factory.createLibLinearRegression(), 
				new RandomSampling(nrModels, calibrationRatio));

		// Wrap the ACP-implementation chosen in Signatures-wrapper
		SignaturesCPRegression signPredictor = factory.createSignaturesCPRegression(predictor, 1, 3);

		// Load data
		try{
			signPredictor.fromChemFile(chemFile.toURI(),"target");
			// or signACP.fromMolsIterator(molsIterator);

			// Train the aggregated ICPs
			signPredictor.train();

			// Save models to skip train again
			signPredictor.save(tempModel);

			// Change the nonconformity score and train a new model!
			predictor.setNonconformityMeasure(NonconfMeasureFactory.getAbsDiffMeasureRegression());
			// or acpImpl.setNonconformityMeasure(NonconfMeasureFactory.getLogNormalizedMeasureRegression(0.1));

			// Train the aggregated ICPs
			signPredictor.train();

			// Save models to skip train again
			signPredictor.save(tempModelDifferentNonconfMeasure);

		} catch(IllegalAccessException e){
			// License not supporting training functionality 
			Utils.writeErrAndExit(e.getMessage());
		} catch (IOException e){
			Utils.writeErrAndExit("Problem loading data or saving models");
		}
	}

	/**
	 * Loads previously created models and use them to predict
	 * @throws IllegalAccessException 
	 */
	public void predict() throws IllegalAccessException {

		// Wrap the ACP-implementation chosen in Signatures-wrapper
		SignaturesCPRegression signPredictor = null;
		SignaturesCPRegression signPredictor_abs_diff = null; 
		// Load models previously trained
		try {
			signPredictor = (SignaturesCPRegression) BNDLoader.loadModel(tempModel.toURI(), null);
			
			signPredictor_abs_diff = (SignaturesCPRegression) BNDLoader.loadModel(
					tempModelDifferentNonconfMeasure.toURI(), null);
		} catch (IOException | InvalidKeyException | IllegalArgumentException e){
			// Could not load precomputed models
			System.err.println(e.getMessage());
			Utils.writeErrAndExit("Could not laod models previously trained");
		}


		// Predict a new example
		try{
			IAtomContainer testMol = CPSignFactory.parseSMILES(smilesToPredict);
			List<Double> confidences = Arrays.asList(0.5, 0.7, 0.9);
			List<CPRegressionResult> regResult = signPredictor.predict(testMol, confidences);
			List<CPRegressionResult> regResult_abs_diff = signPredictor_abs_diff.predict(testMol, confidences);
			for (int i=0; i<confidences.size(); i++){
				System.out.println("Confidence: " + regResult.get(i).getConfidence() + ", interval (normal): " + regResult.get(i).getInterval() + ", interval (abs diff): " + regResult_abs_diff.get(i).getInterval());
			}

			//Predict interval specified as distance to predicted value
			List<CPRegressionResult> distanceResult = signPredictor.predictDistances(testMol, Arrays.asList(0.001));
			System.out.println("Distance prediction: " + distanceResult.get(0));

			// Predict the SignificantSignature
			SignificantSignature ss = signPredictor.predictSignificantSignature(testMol);
			System.out.println(ss);
		} catch (IllegalAccessException e){
			Utils.writeErrAndExit(e.getMessage());
		} catch (CDKException e){
			Utils.writeErrAndExit("CDK Exception when configuring the IAtomContainer");
		}
	}

	public void crossvalidate() throws IllegalAccessException {
		// Chose your predictor and scoring algorithm
		ACPRegression predictor = factory.createACPRegression(
				factory.createLibLinearRegression(), 
				new RandomSampling(nrModels, calibrationRatio)); 

		// Wrap the predictor in Signatures-wrapper
		SignaturesCPRegression signPredictor = factory.createSignaturesCPRegression(predictor, 1, 3);

		// Load data (do not have to load data separately for cross-validate and train/predict-part!)
		try{
			signPredictor.fromChemFile(chemFile.toURI(), "target");

			//Do cross-validation with 10 folds

			List<CVMetric> result = signPredictor.crossvalidate(cvFolds, cvConfidence);
			System.out.println("Cross-validation with " + cvFolds + " folds and conficence " + crossValidationConfidence +": " + result);
		} catch(IllegalAccessException e){
			Utils.writeErrAndExit(e.getMessage());
		} catch (IOException e){
			Utils.writeErrAndExit("Could not load the datafile");
		}
	}



}
