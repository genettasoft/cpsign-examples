package com.genettasoft.cpsign.examples;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import javax.naming.CannotProceedException;

import com.genettasoft.auth.InvalidLicenseException;
import com.genettasoft.modeling.CPSignFactory;
import com.genettasoft.modeling.cheminf.SignaturesCPRegression;
import com.genettasoft.modeling.gridsearch.GridSearch;
import com.genettasoft.modeling.gridsearch.GridSearch.OptimizationType;
import com.genettasoft.modeling.gridsearch.GridSearchException;
import com.genettasoft.modeling.gridsearch.GridSearchResult;
import com.genettasoft.modeling.ml.cp.acp.ACPRegression;
import com.genettasoft.modeling.ml.cp.nonconf.NonconfMeasureFactory;
import com.genettasoft.modeling.ml.cp.nonconf.NonconfMeasureRegression;
import com.genettasoft.modeling.ml.ds_splitting.RandomSampling;

public class ParameterTuningRegression {

	CPSignFactory factory;
	File chemFile;


	/**
	 * Parameters to play around with 
	 */
	int nrCVFolds = 10;
	double crossValidationConfidence = 0.7;
	private int nrModels=5;
	private double calibrationRatio = 0.2;
	private NonconfMeasureRegression nonconfMeasure = NonconfMeasureFactory.getLogNormalizedMeasureRegression(0.0);
	private List<Double> betaValuesToTry = Arrays.asList(0.0, 0.25, 0.5);
	private double tolerance = 0.07;


	public static void main(String[] args) throws CannotProceedException, IllegalArgumentException, IllegalAccessException, IOException, GridSearchException {
		ParameterTuningRegression acp = new ParameterTuningRegression();
		acp.intialise();
		acp.tuneParameters();
		System.out.println("Finished Example Tune Parameters");
	}


	/**
	 * This method just initializes some variables and the CPSignFactory. Please change the 
	 * initialization of CPSignFactory to point to your active license. Also change the 
	 * model and signature-files into a location on your machine so that they can be used 
	 * later on, now temporary files are created for illustrative purposes. 
	 */
	public void intialise() {
		// Start with instantiating CPSignFactory with your license
		try{
			factory = new CPSignFactory(this.getClass().getResource(Configuration.STANDARD_LICENSE).toURI());
		} catch(IOException | InvalidLicenseException | URISyntaxException e){
			// Could not read the license
			Utils.writeErrAndExit(e.getMessage());
		}

		chemFile = new File(this.getClass().getResource("/resources/data/dataset_glucocorticoid.csv.smi.sdf.std_regr_nodupl.sdf").getFile());
	}

	
	public void tuneParameters() throws IllegalArgumentException, IllegalAccessException, IOException, GridSearchException {
		// Create a GridSearch object - optionally set nrCV folds, nr ACP models etc..
		GridSearch gs = new GridSearch(nrCVFolds, crossValidationConfidence, tolerance);
		// Set your custom parameter regions
		gs.setC_END(6);
		gs.setBetaValues(betaValuesToTry); // if LogNormalizedNonconfMeasureRegression is not set, the grid search will not be done even if this list is set
		// Set a Writer to write all output to (otherwise only give you the optimal result)
		Writer writer = new OutputStreamWriter(System.out);
		gs.setWriter(writer);
		
		// Chose your predictor and scoring algorithm
		ACPRegression predictor = factory.createACPRegression(
				factory.createLibLinearRegression(), 
				new RandomSampling(nrModels, calibrationRatio));
		
		// Set the nonconformity measure 
		predictor.setNonconformityMeasure(nonconfMeasure);

		// Wrap the predictor in Signatures-wrapper
		SignaturesCPRegression signPredictor = factory.createSignaturesCPRegression(predictor, 1, 3);

		// Load data
		try {
			signPredictor.fromChemFile(chemFile.toURI(), 
					"target");
		} catch (IllegalArgumentException | IOException e) {
			Utils.writeErrAndExit("Could not parse dataset file: " + e.getMessage());
		} catch (IllegalAccessException e){
			Utils.writeErrAndExit("License does not have train access");
		}
		
		
		// Start the Grid Search
		GridSearchResult res = gs.regression(signPredictor, OptimizationType.EFFICIENCY);
		System.out.println(res);
	}
	


}
