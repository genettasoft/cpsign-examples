package com.genettasoft.cpsign.examples;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

public class RunAll {

	
	List<Class<? extends Object>> classesToRun = Arrays.asList(
			ComplexSMILESFiles.class,
			GeneratePredictionImages.class,
			ManipulatingSparseDatasets.class,
			
			NumericACPClassification.class,
			NumericACPRegression.class,
			NumericVAPClassification.class,
			NumericTCPClassification.class,
			
			ParameterTuningClassification.class,
			ParameterTuningRegression.class,
			RegressionChangeNonconformityScore.class,
			
			SettingSignaturesGenerator.class,
			SettingSVMParameters.class,
			
			SignACPClassification.class,
			SignACPRegression.class,
			SignVAPClassification.class,
			SignTCPClassification.class
			
			);
	
	@Test
	public void RunAllExamples() throws Exception{
		
		for(Class<?> clazz : classesToRun){
			try{
				runClass(clazz);
				System.out.println("\n--------------------------------------------------------------------------------------------\n");
			} catch(Exception e){
				System.err.println("Class " + clazz + " failed with exception stacktrace:");
				e.printStackTrace();
			} catch(Error e){
				System.err.println("Class " + clazz + " failed with error stacktrace:");
				e.printStackTrace();
			}
		}
	}
	
	public static void runClass(Class<?> clazz) throws Exception {
		Method mainMethod = clazz.getMethod("main", String[].class);
		mainMethod.invoke(clazz.newInstance(), (Object)null);
	}
	
	@Rule
	public final ExpectedSystemExit exit = ExpectedSystemExit.none();
	
	@Test
	public void runExtendCLI() {
		exit.expectSystemExitWithStatus(0); // should call exit with a successful exit status
		
		ExtendCLI.main(new String[] {});
	}
}
